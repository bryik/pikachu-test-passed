Usage:
===
Run `pikachu-test-passed [smaller | s | smallerColoured | sc]` after your test passed, and you will surprised Pikachu.

Happy testing :)
